# ADPluginEdge conda recipe

Home: "https://github.com/areaDetector/ADPluginEdge"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADPluginEdge module
